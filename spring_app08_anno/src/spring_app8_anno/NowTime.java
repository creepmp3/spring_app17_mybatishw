package spring_app8_anno;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NowTime{
    
    public String getTime(){
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd aa hh시mm분ss초 ");
        String now = sdf.format(d);
        return now;
    }
}
