package kr.co.hbilab.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TestMain {
    public static void main(String[] args) {
        
        ApplicationContext ctx = new GenericXmlApplicationContext("app2.xml");
        Message m = ctx.getBean("message", Message.class);
        
        m.printMsg();

    }
}
