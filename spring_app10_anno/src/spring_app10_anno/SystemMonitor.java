package spring_app10_anno;

import javax.annotation.Resource;

public class SystemMonitor implements Monitor{
    
    /** ***********************************************
     의존관계 주입을 자동으로 처리
     by type : type을 기준으로 객체인지 검사해서 처리
     
     @Autowired
     멤버변수와 setter에 줄수있다
     멤버변수에 적용하면 setter는 생략가능
     
     
     @Autowired(required=true) : 반드시 값이 있어야 하고 기본값이 true
     @Autowired(required=false) : 자동으로 값을 찾고 없으면 값을 지정하지 않는다
    ************************************************ */
    
    //@Autowired(required=false)
    //@Qualifier("test1")
    Sender sender;
    
    public SystemMonitor(){
    }

    //@Autowired
    @Resource(name="sender2")
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public SystemMonitor(Sender sender) {
        super();
        this.sender = sender;
    }

    @Override
    public void showMonitor() {
        if(sender!=null){
            sender.show();
        }
    }
    
}
